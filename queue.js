let collection = [];

function print(){
    if(collection.length == 0){
        return collection
    }
    return false
}
function enqueue(item){
    collection.push(item)
    return collection
}

function dequeue(item){
    collection.shift()
    return collection
}

function isEmpty(){
    if(collection.length == 0){
        return collection = []
    }
    return false
}

function front(){
    let head = collection.shift()
    collection.unshift(head)
    return head

}

function size(){
    return collection.length
}
// Write the queue functions below.

module.exports = {
    enqueue,
    dequeue,
    isEmpty,
    front,
    size,
    print
};